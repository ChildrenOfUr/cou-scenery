# CoU Scenery

The scenery files here the ones that were used to build the streets in CoU (as of Feb 1, 2024). If different asset sizes were needed, we ran the files in scenery through image magick to create the ones in scenery_rezized (named by size needed).
